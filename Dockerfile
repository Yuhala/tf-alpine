# Copyright 2018 ML2Grow BVBA
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
FROM alpine:3.7

ENV JAVA_HOME /usr/lib/jvm/java-1.8-openjdk
ENV BAZEL_VERSION 0.10.0
ENV TENSORFLOW_VERSION 1.7.0

RUN apk update \
    && apk add --no-cache --virtual=.builddeps \
        bash \
        build-base \
        linux-headers \
        make \
        musl-dev \
        openjdk8 \
        wget \
        zip \
        git \ 
        curl 
RUN apk --no-cache add \
        python3 \
        python3-tkinter \
        jemalloc \
        libc6-compat \
        libexecinfo \
        libunwind \
    && apk --no-cache add --virtual .builddeps.1 \
        libexecinfo-dev \
        libunwind-dev \
        patch \
        perl \
        sed \
    && cd /tmp \
    && $(ln -s /usr/bin/python3 /usr/bin/python) 

ADD tf-${TENSORFLOW_VERSION}-alpine.patch /tmp/ 

# Bazel download
RUN curl -SLO https://github.com/bazelbuild/bazel/releases/download/${BAZEL_VERSION}/bazel-${BAZEL_VERSION}-dist.zip \
    && mkdir bazel-${BAZEL_VERSION} \
    && unzip -qd bazel-${BAZEL_VERSION} bazel-${BAZEL_VERSION}-dist.zip

# Bazel install
RUN cd bazel-${BAZEL_VERSION} \
    && sed -i -e 's/-classpath/-J-Xmx8192m -J-Xms128m -classpath/g' scripts/bootstrap/compile.sh \
    && bash compile.sh \
    && cp -p output/bazel /usr/bin/

# Download Tensorflow
RUN cd /tmp \
    && curl -SL https://github.com/tensorflow/tensorflow/archive/v${TENSORFLOW_VERSION}.tar.gz \
        | tar xzf -


# Build Tensorflow
RUN cd /tmp/tensorflow-${TENSORFLOW_VERSION} \
    && : musl-libc does not have "secure_getenv" function \
    && sed -i -e '/JEMALLOC_HAVE_SECURE_GETENV/d' third_party/jemalloc.BUILD \
    && sed -i -e '/define TF_GENERATE_BACKTRACE/d' tensorflow/core/platform/default/stacktrace.h \
    && sed -i -e '/define TF_GENERATE_STACKTRACE/d' tensorflow/core/platform/stacktrace_handler.cc \
    && patch -s -p1 < ../tf-${TENSORFLOW_VERSION}-alpine.patch \
    && cd /tmp/tensorflow-${TENSORFLOW_VERSION}/tensorflow/cc && git clone https://gitlab.com/Yuhala/heart-model.git \
    && cd /tmp/tensorflow-${TENSORFLOW_VERSION} \
    && PYTHON_BIN_PATH=/usr/bin/python \
        PYTHON_LIB_PATH=/usr/lib/python3.6/site-packages \
        CC_OPT_FLAGS="-march=native" \
        TF_NEED_JEMALLOC=1 \
        TF_NEED_GCP=0 \
        TF_NEED_HDFS=0 \
        TF_NEED_S3=0 \
        TF_ENABLE_XLA=0 \
        TF_NEED_GDR=0 \
        TF_NEED_VERBS=0 \
        TF_NEED_OPENCL=0 \
        TF_NEED_CUDA=0 \
        TF_NEED_MPI=0 \
        bash configure

RUN cd /tmp/tensorflow-${TENSORFLOW_VERSION} \
    && bazel build -c opt //tensorflow/cc/heart-model 
    
#CMD ["python3"]
